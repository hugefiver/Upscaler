default: black isort mypy

# Format with black formatter
black:
    black upscaler/

# Sort imports using isort
isort:
    isort upscaler/ --profile black

# Check typings with mypy
mypy:
    mypy --strict --pretty upscaler/