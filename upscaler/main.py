# main.py: main application
#
# Copyright (C) 2022 Upscaler Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-only

import gi

gi.require_version("Gtk", "4.0")
gi.require_version("Adw", "1")

import sys
from gettext import gettext as _
from typing import Any, Callable, Optional, cast

from gi.repository import Adw, Gio, GLib, Gtk

from upscaler.app_profile import (  # type: ignore
    APP_ICON_NAME,
    APP_ID,
    APP_NAME,
    APP_VERSION,
)
from upscaler.window import UpscalerWindow


class UpscalerApplication(Adw.Application):
    def __init__(self) -> None:
        super().__init__(
            application_id=APP_ID,
            flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE,
            resource_base_path="/io/gitlab/theevilskeleton/Upscaler",
        )
        self.create_action("quit", self.__quit, ["<primary>q"])
        self.create_action("about", self.__about_action)
        self.create_action("open", self.__open_file, ["<primary>o"])
        self.create_action(
            "open-output", self.__open_output, param=GLib.VariantType("s")
        )
        self.file: Optional[str] = None

    def do_activate(self) -> None:
        """
        Call when the application is activated.

        We raise the application's main window, creating it if
        necessary.
        """
        win: UpscalerWindow = cast(UpscalerWindow, self.props.active_window)
        if not win:
            win = UpscalerWindow(application=self)
        win.present()
        if self.file is not None:
            print(self.file)
            win.on_load_file(Gio.File.new_for_path(self.file))

    def __open_file(self, *args: Any) -> None:
        cast(UpscalerWindow, self.props.active_window).open_file()

    def __open_output(self, action: Gio.SimpleAction, data: GLib.Variant) -> None:
        file_path: str = data.unpack()
        file = open(file_path, "r")
        fid = file.fileno()
        connection = Gio.bus_get_sync(Gio.BusType.SESSION, None)
        proxy = Gio.DBusProxy.new_sync(
            connection,
            Gio.DBusProxyFlags.NONE,
            None,
            "org.freedesktop.portal.Desktop",
            "/org/freedesktop/portal/desktop",
            "org.freedesktop.portal.OpenURI",
            None,
        )

        try:
            proxy.call_with_unix_fd_list_sync(
                "OpenFile",
                GLib.Variant("(sha{sv})", ("", 0, {"ask": GLib.Variant("b", True)})),
                Gio.DBusCallFlags.NONE,
                -1,
                Gio.UnixFDList.new_from_array([fid]),
                None,
            )
        except Exception as e:
            print(f"Error: {e}")

    def __about_action(self, *args: Any) -> None:
        """Open About window."""
        about = Adw.AboutWindow(
            transient_for=cast(Gtk.Window, self.props.active_window),
            application_name=APP_NAME,
            application_icon=APP_ICON_NAME,
            developer_name=_("Upscaler Contributors"),
            version=APP_VERSION,
            artists=["Frostbitten-jello https://github.com/Frostbitten-jello"],
            developers=developers_list(),
            translator_credits=translators_list(),
            copyright=_("Copyright © 2022 Upscaler Contributors"),
            license_type=Gtk.License.GPL_3_0_ONLY,
            website="https://theevilskeleton.gitlab.io/upscaler",
            issue_url="https://gitlab.gnome.org/World/Upscaler/-/issues",
            support_url="https://matrix.to/#/#Upscaler:gnome.org",
            comments=_(
                f'<b><big>Backend</big></b>\n{APP_NAME} uses <a href="https://github.com/xinntao/Real-ESRGAN-ncnn-vulkan">Real-ESRGAN ncnn Vulkan</a> under the hood to upscale and enhance images. Without it, {APP_NAME} wouldn’t exist.\n\n<b><big>Origins of {APP_NAME}</big></b>\n{APP_NAME} started out as a final project for the final <a href="https://www.edx.org/course/introduction-computer-science-harvardx-cs50x">CS50x</a> assignment, Harvard University’s introductory course to computer science. However, it was later decided to continue it as free and open-source software. Without CS50x, {APP_NAME} wouldn’t exist either.'
            ),
        )
        about.add_acknowledgement_section(
            _("Algorithms by"),
            [
                "Real-ESRGAN https://github.com/xinntao/Real-ESRGAN",
                "Real-ESRGAN ncnn Vulkan https://github.com/xinntao/Real-ESRGAN-ncnn-vulkan",
            ],
        )
        about.add_acknowledgement_section(
            _("Code and Design Borrowed from"),
            [
                "Avvie https://github.com/Taiko2k/Avvie",
                "Bottles https://github.com/bottlesdevs/Bottles",
                "Loupe https://gitlab.gnome.org/BrainBlasted/loupe",
                "Totem https://gitlab.gnome.org/GNOME/totem",
                "Fractal https://gitlab.gnome.org/GNOME/fractal",
                "Builder https://gitlab.gnome.org/GNOME/gnome-builder",
            ],
        )
        about.add_acknowledgement_section(
            _("Sample Image from"),
            [
                "Princess Hinghoi https://safebooru.org/index.php?page=post&s=view&id=3084434",
            ],
        )
        about.add_legal_section(
            title="Real-ESRGAN ncnn Vulkan",
            copyright=_("Copyright © 2021 Xintao Wang"),
            license_type=Gtk.License.MIT_X11,
            license=None,
        )
        about.present()

    def create_action(
        self,
        name: str,
        callback: Callable[[Gio.SimpleAction, GLib.Variant], None],
        shortcuts: Optional[list[str]] = None,
        param: Optional[GLib.VariantType] = None,
    ) -> None:
        """
        Add an application action.

        Args:
            name: the name of the action
            callback: the function to be called when the action is
              activated
            shortcuts: an optional list of accelerators
            param: an optional list of parameters for the action
        """
        action = Gio.SimpleAction.new(name, param)
        action.connect("activate", callback)
        self.add_action(action)
        if shortcuts:
            self.set_accels_for_action(f"app.{name}", shortcuts)

    def do_command_line(self, command_line: Gio.ApplicationCommandLine) -> int:
        args = command_line.get_arguments()
        if len(args) > 1:
            self.file = command_line.create_file_for_arg(args[1]).get_path()
        self.activate()
        return 0

    def __quit(self, _args: Any, *args: Any) -> None:
        """Quit application."""
        win = self.props.active_window
        if win:
            win.destroy()


def translators_list() -> str:
    """Translators list. To add yourself into the list, add '\n', followed by
        your name/username, and optionally an email or URL:

    Name only:    \nHari Rana
    Name + URL:   \nHari Rana https://theevilskeleton.gitlab.io
    Name + Email: \nHari Rana <theevilskeleton@riseup.net>
    """
    return _(
        "Jürgen Benvenuti <gastornis@posteo.org>\nPhilip Goto <philip.goto@gmail.com>\nSabri Ünal <libreajans@gmail.com>\nyukidream https://fosstodon.org/@yukidream\nAnatoly Bogomolov <tolya.bogomolov2019@gmail.com>\nÅke Engelbrektson <eson@svenskasprakfiler.se>\nOnuralp SEZER <thunderbirdtr@fedoraproject.org>\nLuna Jernberg <lunajernberg@gnome.org>\nvolkov https://matrix.to/#/@vovkiv:matrix.org"
    )


def developers_list() -> list[str]:
    """Developers/Contributors list. If you have contributed code, feel free
        to add yourself into the Python list:

    Name only:    \nHari Rana
    Name + URL:   \nHari Rana https://theevilskeleton.gitlab.io
    Name + Email: \nHari Rana <theevilskeleton@riseup.net>
    """
    return [
        "Hari Rana (TheEvilSkeleton) https://theevilskeleton.gitlab.io",
        "Matteo",
        "Onuralp SEZER <thunderbirdtr@fedoraproject.org>",
        "Khaleel Al-Adhami <khaleel.aladhami@gmail.com>",
        "gregorni https://gitlab.gnome.org/gregorni",
    ]


def main(version: str) -> int:
    # The application's entry point.
    app = UpscalerApplication()
    return app.run(sys.argv)
