# window.py: main window
#
# Copyright (C) 2022 Upscaler Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-only

import os
import re
import subprocess
import tempfile
from filecmp import cmp
from gettext import gettext as _
from sys import exit
from typing import Any, Callable, Literal, Optional, cast

import vulkan  # type: ignore
from gi.repository import Adw, Gdk, Gio, GLib, Gtk, Pango
from PIL import Image, ImageChops, ImageOps  # type: ignore

from upscaler.app_profile import APP_ICON_NAME  # type: ignore
from upscaler.file_chooser import FileChooser
from upscaler.filters import image_formats
from upscaler.threading import RunAsync

ALG_WARNINGS = ["vkQueueSubmit failed"]

UPSCALE_FACTOR = 4


class AlgorithmFailed(Exception):
    """Raise when the algorithm has failed."""

    def __init__(self, result_code: int, output: str) -> None:
        super().__init__()
        self.result_code = result_code
        self.output = output

    def __str__(self) -> str:
        return (
            f"Algorithm failed.\nResult code: {self.result_code}\nOutput: {self.output}"
        )


class AlgorithmWarning(Exception):
    """Raise when the output could be damaged."""

    pass


@Gtk.Template(resource_path="/io/gitlab/theevilskeleton/Upscaler/gtk/window.ui")
class UpscalerWindow(Adw.ApplicationWindow):
    __gtype_name__ = "UpscalerWindow"

    # Declare child widgets
    toast: Adw.ToastOverlay = Gtk.Template.Child()  # type: ignore
    stack_upscaler: Gtk.Stack = Gtk.Template.Child()  # type: ignore
    status_welcome: Adw.StatusPage = Gtk.Template.Child()  # type: ignore
    button_input: Gtk.Button = Gtk.Template.Child()  # type: ignore
    action_image_size: Adw.ActionRow = Gtk.Template.Child()  # type: ignore
    action_upscale_image_size: Adw.ActionRow = Gtk.Template.Child()  # type: ignore
    button_upscale: Gtk.Button = Gtk.Template.Child()  # type: ignore
    spinner_loading: Gtk.Spinner = Gtk.Template.Child()  # type: ignore
    image: Gtk.Picture = Gtk.Template.Child()  # type: ignore
    # video = Gtk.Template.Child() # type: ignore
    combo_models: Adw.ComboRow = Gtk.Template.Child()  # type: ignore
    string_models: Gtk.StringList = Gtk.Template.Child()  # type: ignore
    # spin_scale = Gtk.Template.Child() # type: ignore
    button_output: Gtk.Button = Gtk.Template.Child()  # type: ignore
    label_output: Gtk.Label = Gtk.Template.Child()  # type: ignore
    button_cancel: Gtk.Button = Gtk.Template.Child()  # type: ignore
    progressbar: Gtk.ProgressBar = Gtk.Template.Child()  # type: ignore

    # Initialize function
    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)

        self.settings = Gio.Settings(schema_id="io.gitlab.theevilskeleton.Upscaler")
        self.set_default_size(
            self.settings.get_int("width"), self.settings.get_int("height")
        )
        if self.settings.get_boolean("is-maximized"):
            self.maximize()

        # Check if hardware is supported
        if os.environ.get("DEBUG_VULKAN_CHECK") != "0":
            GLib.idle_add(self.__vulkaninfo)
        else:
            print("Skipping Vulkan check")

        # Declare App (needed for notifications later)
        app = kwargs.get("application")
        if app is None:
            raise ValueError("Application should be passed to UpscalerWindow")
        self.app: Gio.Application = app

        # Set icon on welcome page
        self.status_welcome.set_icon_name(APP_ICON_NAME)

        # Declare default models and variables
        self.model_images = {
            "realesrgan-x4plus": _("Photo"),
            "realesrgan-x4plus-anime": _("Cartoon/Anime"),
        }

        self.process: Optional[subprocess.Popen[Any]] = None
        self.output_file_path: Optional[str] = None
        self.input_file_path: Optional[str] = None
        content = Gdk.ContentFormats.new_for_gtype(Gio.File)
        self.target = Gtk.DropTarget(formats=content, actions=Gdk.DragAction.COPY)
        self.string_models.splice(0, 0, list(self.model_images.values()))
        self.previous_stack = "stack_welcome_page"

        # Connect signals
        self.connect("notify::is-active", self.__remove_notifications)
        self.button_input.connect("clicked", self.open_file)
        self.button_upscale.connect("clicked", self.__upscale)
        self.button_output.connect("clicked", self.__output_location)
        self.combo_models.connect("notify::selected", self.__set_model)
        self.button_cancel.connect("clicked", self.__cancel)
        self.target.connect("drop", self.__on_drop)
        self.target.connect("enter", self.__on_enter)
        self.target.connect("leave", self.__on_leave)
        self.add_controller(self.target)

        # self.spin_scale.connect('value-changed', self.__update_post_upscale_image_size)

        # self.model_videos = [
        #     'realesr-animevideov3',
        # ]

    def __on_file_open(
        self, original_input_file_path: str, input_file_path: str, texture: Gdk.Texture
    ) -> None:
        """Open and display file."""
        self.original_input_file_path = original_input_file_path
        self.input_file_path = input_file_path
        self.image_size = (texture.get_width(), texture.get_height())

        # Display image
        self.action_image_size.set_subtitle(
            f"{self.image_size[0]} × {self.image_size[1]}"
        )
        self.action_upscale_image_size.set_subtitle(
            f"{self.image_size[0] * UPSCALE_FACTOR} × {self.image_size[1] * UPSCALE_FACTOR}"
        )
        self.image.set_paintable(texture)

        # Reset widgets
        self.label_output.set_label(_("(None)"))
        self.button_upscale.set_sensitive(False)
        self.button_upscale.set_has_tooltip(True)
        self.combo_models.set_selected(0)
        self.stack_upscaler.set_visible_child_name("stack_upscale")
        self.previous_stack = "stack_upscale"
        self.spinner_loading.stop()

    def open_file(self, *args: Any) -> None:
        """Open the file chooser to load the file."""
        FileChooser.open_file(self)

    def __output_location(self, *args: Any) -> None:
        """
        Select output file location.

        Widgets are updated to let the user continue
        """

        def good(output_file_path: str) -> None:
            # Set variables
            self.output_file_path = output_file_path

            # Update widgets
            self.button_upscale.set_sensitive(True)
            self.button_upscale.set_has_tooltip(False)

            # Trim long base name if necessary
            self.label_output.set_label(os.path.basename(self.output_file_path))
            self.label_output.set_ellipsize(Pango.EllipsizeMode.MIDDLE)

        def bad(message: Optional[str]) -> None:
            if message:
                self.toast.add_toast(Adw.Toast.new(message))

        if self.original_input_file_path is None:
            return

        base_path = os.path.basename(os.path.splitext(self.original_input_file_path)[0])
        image_size = [x * UPSCALE_FACTOR for x in self.image_size]
        FileChooser.output_file(
            self, f"{base_path}-{image_size[0]}x{image_size[1]}-upscaled.png", good, bad
        )

    def __on_drop(self, _: Any, file: Gio.File, *args: Any) -> None:
        """Load file when it has been dropped into the app."""
        self.on_load_file(file)

    def __on_enter(self, *args: Any) -> Literal[Gdk.DragAction.COPY]:
        """Display drag & drop stack page."""
        self.stack_upscaler.set_visible_child_name("stack_drop")
        return Gdk.DragAction.COPY

    def __on_leave(self, *args: Any) -> None:
        """Display previous stack page."""
        self.stack_upscaler.set_visible_child_name(self.previous_stack)

    def __upscale_progress(self, progress: float) -> None:
        """Updates upscale progress."""
        if self.stack_upscaler.get_visible_child_name() == "stack_upscaling":
            self.set_progress(progress)

    def __upscale(self, *args: Any) -> None:
        """Initialize algorithm and updates widgets."""
        # Since GTK is not thread safe, prepare some data in the main thread
        self.cancelled = False

        # Appropriately close child windows
        def reset_widgets() -> None:
            self.button_upscale.set_sensitive(True)
            self.progressbar.set_text(_("Loading…"))
            self.progressbar.set_fraction(0)
            self.cancelled = False

        # Run in a separate thread
        def run() -> None:
            if self.input_file_path is None or self.output_file_path is None:
                raise AlgorithmFailed(
                    0, _("Unexpected error while running the algorithm")
                )

            command: list[str] = [
                "realesrgan-ncnn-vulkan",
                # fmt: off
                "-i", self.input_file_path,
                "-o", self.output_file_path,
                "-n", list(self.model_images)[self.combo_models.get_selected()],
                "-s", "4",
                # fmt: on
            ]

            self.process = subprocess.Popen(
                command, stderr=subprocess.PIPE, universal_newlines=True
            )
            print("Running: ", end="")
            print(*command)

            # Read each line, query the percentage and update the progress bar
            output = ""
            bad = False
            if self.process.stderr is not None:
                for line in iter(self.process.stderr.readline, ""):
                    print(line, end="")
                    output += line
                    res = re.match("^(\d*.\d+)%$", line)
                    if res:
                        GLib.idle_add(self.__upscale_progress, float(res.group(1)))
                    else:
                        # Check if this line is a warning
                        if bad:
                            continue
                        for warn in ALG_WARNINGS:
                            if re.match(warn, line) is not None:
                                bad = True
                                continue

            # Process algorithm output
            result = self.process.poll()
            if result != 0 or result is None:
                if result is None:
                    result = 0
                raise AlgorithmFailed(result, output)

            if bad:
                raise AlgorithmWarning

        # Run after run() function finishes
        def callback(result: Gio.AsyncResult, error: Optional[Exception]) -> None:
            if self.cancelled == True:
                self.toast.add_toast(Adw.Toast.new(_("Upscaling Cancelled")))
            else:
                self.upscaling_completed_dialog(error)

            self.stack_upscaler.set_visible_child_name("stack_upscale")
            self.previous_stack = "stack_upscale"
            reset_widgets()

        # Run functions asynchronously
        RunAsync(run, callback)
        self.stack_upscaler.set_visible_child_name("stack_upscaling")
        self.previous_stack = "stack_upscaling"
        self.button_upscale.set_sensitive(False)

    def upscaling_completed_dialog(self, error: Optional[Exception]) -> None:
        """Ask the user if they want to open the file."""
        if self.output_file_path is None:
            return

        toast = None

        notification = Gio.Notification()
        notification.set_body(_(f"Upscaled {os.path.basename(self.output_file_path)}"))

        output_file_variant = GLib.Variant("s", self.output_file_path)

        # Display success
        if error is None:
            toast = Adw.Toast(
                title=_("Image upscaled"),
                button_label=_("Open"),
                action_name="app.open-output",
                action_target=output_file_variant,
                timeout=0,
            )
            self.toast.add_toast(toast)

            notification.set_title(_("Upscaling Completed"))

            notification.set_default_action_and_target(
                "app.open-output", output_file_variant
            )
            notification.add_button_with_target(
                _("Open"), "app.open-output", output_file_variant
            )

        # Display success with warnings
        elif isinstance(error, AlgorithmWarning):
            toast = Adw.Toast(
                title=_("Image upscaled with warnings"),
                button_label=_("Open"),
                action_name="app.open-output",
                action_target=output_file_variant,
                timeout=0,
            )
            self.toast.add_toast(toast)

            notification.set_title(_("Upscaling Completed with Warnings"))

            notification.set_default_action_and_target(
                "app.open-output", output_file_variant
            )
            notification.add_button_with_target(
                _("Open"), "app.open-output", output_file_variant
            )

        # Display error dialog with error message
        else:
            dialog = Adw.MessageDialog.new(self, _("Error while processing"), None)
            sw = Gtk.ScrolledWindow()
            sw.set_min_content_height(200)
            sw.set_min_content_width(400)
            sw.add_css_class("card")

            text = Gtk.Label()
            text.set_label(str(error))
            text.set_margin_top(12)
            text.set_margin_bottom(12)
            text.set_margin_start(12)
            text.set_margin_end(12)
            text.set_xalign(0)
            text.set_yalign(0)
            text.add_css_class("monospace")
            text.set_wrap(True)
            text.set_wrap_mode(Pango.WrapMode.WORD_CHAR)

            sw.set_child(text)
            dialog.set_extra_child(sw)

            def error_response(dialog: Adw.MessageDialog, response_id: str) -> None:
                if response_id == "copy":
                    display = Gdk.Display.get_default()
                    if display is None:
                        return
                    clipboard = display.get_clipboard()
                    clipboard.set(str(error))
                    toast = Adw.Toast.new(_("Error copied to clipboard"))
                    self.toast.add_toast(toast)
                dialog.close()

            dialog.add_response("copy", _("_Copy to clipboard"))
            dialog.set_response_appearance("copy", Adw.ResponseAppearance.SUGGESTED)
            dialog.add_response("ok", _("_Dismiss"))
            dialog.connect("response", error_response)
            dialog.present()

            notification.set_title(_("Upscaling Failed"))
            notification.set_body(_("Error while processing"))

        if not self.props.is_active:
            self.app.send_notification("upscaling-done", notification)

    def __set_model(self, *args: Any) -> None:
        """Set model and print."""
        print(
            _("Model name: {}").format(
                list(self.model_images)[self.combo_models.get_selected()]
            )
        )

    # Update post-upscale image size as the user adjusts the spinner
    # def __update_post_upscale_image_size(self, *args):
    #     upscale_image_size = [
    #         self.image_size[1] * int(self.spin_scale.get_value()),
    #         self.image_size[2] * int(self.spin_scale.get_value()),
    #     ]
    #     self.action_upscale_image_size.set_subtitle(f'{upscale_image_size[0]} × {upscale_image_size[1]}')

    def set_progress(self, progress: float) -> None:
        """Update progress widget."""
        self.progressbar.set_text(str(progress) + " %")
        self.progressbar.set_fraction(progress / 100)

    def close_dialog(self, function: Callable[[], None]) -> None:
        """Prompt the user to stop the algorithm when it is running."""
        self.stop_upscaling_dialog = Adw.MessageDialog.new(
            self,
            _("Stop upscaling?"),
            _("You will lose all progress."),
        )

        def response(dialog: Adw.MessageDialog, response_id: str) -> None:
            if response_id == "stop":
                function()

        self.stop_upscaling_dialog.add_response("cancel", _("_Cancel"))
        self.stop_upscaling_dialog.add_response("stop", _("_Stop"))
        self.stop_upscaling_dialog.set_response_appearance(
            "stop", Adw.ResponseAppearance.DESTRUCTIVE
        )
        self.stop_upscaling_dialog.connect("response", response)
        self.stop_upscaling_dialog.present()

    def start_loading(self) -> None:
        """Show loading screen."""
        self.stack_upscaler.set_visible_child_name("stack_loading")
        self.spinner_loading.start()

    def __cancel(self, *args: Any) -> None:
        """Stop algorithm."""

        def function() -> None:
            self.cancelled = True
            if self.process:
                self.process.kill()

        self.close_dialog(function)

    def __load_file(self, file: Gio.File) -> tuple[Gdk.Texture, str, str]:
        """Attempt to load image."""
        # Suppress "DecompressionBombWarning" warning
        # https://pillow.readthedocs.io/en/stable/reference/Image.html#PIL.Image.open
        Image.MAX_IMAGE_PIXELS = None

        img = Image.open(file.get_path())

        # Check if file is supported
        if img.get_format_mimetype() not in image_formats:
            raise Exception(f"unsupported image {file.get_path()}")

        img, file_path = self.__transpose(file.get_path(), img)

        img = img.convert("RGBA")

        width, height = img.size

        texture: Gdk.Texture = Gdk.MemoryTexture.new(
            width,
            height,
            Gdk.MemoryFormat.R8G8B8A8,
            GLib.Bytes.new(img.tobytes()),
            # "Stride" is the amount of bytes per row in a given image.
            # The R8G8B8A8 format is 4 bytes long (each channel is a byte (8-bits)), so we multiply the width by 4 to obtain the stride.
            width * 4,
        )

        return (texture, cast(str, file.get_path()), file_path)

    def __transpose(self, file_path: Optional[str], img: Image) -> Image:
        """Transpose image and compare them."""
        img_transposed = ImageOps.exif_transpose(img)
        diff = ImageChops.difference(img, img_transposed)

        if diff.getbbox():
            # Creates temporary file to store transposed image
            file_path = tempfile.NamedTemporaryFile(suffix=f".{img.format}").name
            img_transposed.save(file_path, quality=100, subsampling=0)
            img = img_transposed
            print(f"Transposing image and saving temporarily to “{file_path}”")

        return (img, file_path)

    def on_load_file(self, file: Gio.File) -> None:
        """Load a given file."""

        def callback(
            items: tuple[Gdk.Texture, str, str], error: Optional[Exception]
        ) -> None:
            texture, original_file_path, file_path = items

            if error:
                self.toast.add_toast(
                    Adw.Toast.new(
                        _(
                            f"“{os.path.basename(file.get_path() or '')}” is not a valid image."
                        )
                    )
                )
                self.stack_upscaler.set_visible_child_name(self.previous_stack)
            else:
                self.__on_file_open(original_file_path, file_path, texture)

        if self.__compare(self.input_file_path, file.get_path()):
            return

        self.start_loading()
        print(f"Input file: {file.get_path()}")

        RunAsync(self.__load_file, callback, file)

    def __compare(self, file1: Optional[str], file2: Optional[str]) -> bool:
        """Compare two files."""
        if file1 and file2:
            return cmp(file1, file2)
        return False

    def __vulkaninfo(self) -> None:
        """Check if Vulkan works."""

        def response(dialog: Any, response_id: str) -> None:
            exit(1)

        try:
            vulkan.vkCreateInstance(vulkan.VkInstanceCreateInfo(), None)
        except (vulkan.VkErrorIncompatibleDriver, OSError):
            print("Error: Vulkan drivers not found")
            dialog = Adw.MessageDialog.new(
                self,
                _("Incompatible or Missing Vulkan Drivers"),
                # fmt: off
                _("The Vulkan drivers are either not installed or incompatible with the hardware. Please make sure that the correct Vulkan drivers are installed for the appropriate hardware."),
                # fmt: on
            )
            dialog.add_response("exit", _("_Exit Upscaler"))
            dialog.connect("response", response)
            dialog.present()

    def __remove_notifications(self, *args: Any) -> None:
        self.app.withdraw_notification("upscaling-done")

    def do_close_request(self) -> bool:
        """Prompt user to stop the algorithm if it's running."""
        if self.stack_upscaler.get_visible_child_name() == "stack_upscaling":

            def function() -> None:
                """Save window size"""
                self.settings.set_int("width", self.get_width())
                self.settings.set_int("height", self.get_height())
                self.settings.set_boolean("is-maximized", self.is_maximized())

                exit()

            self.close_dialog(function)
            return True
        return False
